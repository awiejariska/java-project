package com.assignment.service;

import java.util.List;
import java.util.Optional;

import com.assignment.model.SouvenirModel;

public interface SouvenirService {
	List<SouvenirModel> getAllSouvenir();
	
	void delete(Long id);
	
	SouvenirModel save(SouvenirModel Souvenir);
	
	Optional<SouvenirModel> findById(Long id);
	
	List<SouvenirModel> CariSouvenir(String code, String name, String createdBy);
}
