package com.assignment.restcontroller;

import java.text.ParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.model.SouvenirModel;
import com.assignment.service.SouvenirService;

@RestController
@RequestMapping(path="/api/souvenir", produces="application/json")
@CrossOrigin(origins = "*")
public class SouvenirRestController {
	@Autowired
	private SouvenirService Souvenirs;
	
	@GetMapping("/")
	public ResponseEntity<?>getAllSouvenir(){
		return new ResponseEntity<>(Souvenirs.getAllSouvenir(), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> saveSouvenir(@RequestBody SouvenirModel souvenir){
		return new ResponseEntity<>(Souvenirs.save(souvenir), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteUnit(@PathVariable("id") Long id) {
		try {
			Souvenirs.delete(id);
		} catch (EmptyResultDataAccessException e) {
			// TODO: handle exception
		}
	}
	
	@PutMapping("/put")
	public ResponseEntity<?> EditSouvenir(@RequestBody SouvenirModel souvenir) {
		return new ResponseEntity<>(Souvenirs.save(souvenir), HttpStatus.OK);
	}
	
	@GetMapping("/CariSouvenir")
	public ResponseEntity<?> CariSouvenir(
			@RequestParam String code,
			@RequestParam String name,
			@RequestParam String createdBy) throws ParseException 
	{
		/* Date dat1 = new SimpleDateFormat("yyyy-MM-dd").parse(createdDate); */
		return new ResponseEntity<>(Souvenirs.CariSouvenir(code,name,createdBy), HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<?> findById(@PathVariable("id") Long id) {
		return new ResponseEntity<>(Souvenirs.findById(id), HttpStatus.OK);
	}
}
