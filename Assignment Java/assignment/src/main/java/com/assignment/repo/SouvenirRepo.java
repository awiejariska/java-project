package com.assignment.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.assignment.model.SouvenirModel;

@Repository
public interface SouvenirRepo extends JpaRepository<SouvenirModel, Long> {
	@Query("select s "
			+ "from SouvenirModel s where isDelete = false")
	List<SouvenirModel> getAllSouvenir();
	
	@Query("select s "
			  + " from SouvenirModel s "
			  + " where s.isDelete=false and  (s.code like %?1% and s.name like %?2% "
			  + "  and s.createdBy like %?3%  )  ") 
	List<SouvenirModel> CariSouvenir(String code, String name,  String createdBy);
	
}
