package com.assignment.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.PostPersist;

@Entity
@Table(name="unit")
public class UnitModel extends Entity_ {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name ="id", length = 11, nullable = false)
	private Long id;
	
	@Column(name="code", length = 50, nullable = false)
	private String code;
	
	@PostPersist
	public void generateCodeUnit() {
		if(id.toString().length() == 1) code = ("UN000" + id);
		else if(id.toString().length() == 2) code = ("UN00" + id);
		else if(id.toString().length() == 3) code = ("UN0" + id);
		else if(id.toString().length() == 4) code = ("UN" + id);
	}
	
	@Column(name="name", length = 50, nullable = false)
	private String name;
	
	@Column(name="description", length = 255)
	private String description;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
