package com.assignment.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Entity_ {
	@Column(name="is_delete", nullable = false)
	private Boolean isDelete = false;
	
	@Column(name="created_by", length = 50, nullable = false)
	private String createdBy;
	
	@Column(name="created_date", nullable = false)
	private LocalDate createdDate;
	
	@Column(name="updated_by", length = 50, nullable = true)
	private String updatedBy;
	
	@Column(name="updated_date", nullable = true)
	private LocalDate updateDate;

	public Boolean getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Boolean isDelete) {
		this.isDelete = isDelete;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDate getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(LocalDate createdDate) {
		this.createdDate = createdDate;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public LocalDate getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(LocalDate updateDate) {
		this.updateDate = updateDate;
	}
	
}
