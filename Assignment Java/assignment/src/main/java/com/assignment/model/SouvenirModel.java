package com.assignment.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PostPersist;
import javax.persistence.Table;

@Entity
@Table(name="souvenir")
public class SouvenirModel extends Entity_ {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name ="id", length = 11, nullable = false)
	private Long id;
	
	@Column(name="code", length = 50, nullable = false)
	private String code;
	
	@PostPersist
	public void generateCodeUnit() {
		if(id.toString().length() == 1) code = ("SV000" + id);
		else if(id.toString().length() == 2) code = ("SV00" + id);
		else if(id.toString().length() == 3) code = ("SV0" + id);
		else if(id.toString().length() == 4) code = ("SV" + id);
	}
	
	@Column(name="name", length = 50, nullable = false)
	private String name;
	
	@Column(name="description", length = 255)
	private String description;
	
	@Column(name="m_unit_id", length = 11, nullable = false)
	private Integer mUnitId;
	
	@ManyToOne
	@JoinColumn(name="m_unit_id", insertable = false, updatable = false)
	public UnitModel unit;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getmUnitId() {
		return mUnitId;
	}

	public void setmUnitId(Integer mUnitId) {
		this.mUnitId = mUnitId;
	}

	public UnitModel getUnit() {
		return unit;
	}

	public void setUnit(UnitModel unit) {
		this.unit = unit;
	}
	
	
}
