package com.assignment.serviceImpl;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.assignment.model.UnitModel;
import com.assignment.repo.UnitRepo;
import com.assignment.service.UnitService;

@Service
public class UnitServiceImpl implements UnitService {
	@Autowired
	private UnitRepo unitRepo;
	
	@Override
	public List<UnitModel> getAllUnit() {
		// TODO Auto-generated method stub
		return unitRepo.getAllUnit();
	}
	
	@Override
	public UnitModel save(UnitModel Unit) {
		// TODO Auto-generated method stub
		return unitRepo.save(Unit);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		unitRepo.deleteById(id);
	}
	
	@Override
	public Optional<UnitModel> findById(Long id) {
		// TODO Auto-generated method stub
		return unitRepo.findById(id);
	}

	@Override
	public List<UnitModel> CariUnit(String code, String name,  String createdBy) {
		// TODO Auto-generated method stub
		return unitRepo.CariUnit(code, name,  createdBy);
	}
}
